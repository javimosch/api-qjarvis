
//global stuff
g = {};

angular.module("QJarvisApp", [
    "QJLoginModule",
    "QJApiModule",
    'QJDirectives',
    "QJConfigModule",
    "QJRouteModule",
    'QJHelperFunctionsModule',
    'QJErrorHandlerModule',
    'QJLoggerModule',
    'QJControlsModule',
    'QJUserModule',
    'QJUsergroupModule',
    'QJMenuModule',
    'QJProfileModule',
    'QJChatModule',
    'QJSettingsModule',
    'QJVipsterModule',
    'QJProjectModule',
    'QJProjectHoursModule',
    'ui.bootstrap.datetimepicker'
])

.config(['$httpProvider', '$sceDelegateProvider',
    function($httpProvider, $sceDelegateProvider) {
        $httpProvider.defaults.useXDomain = true;
        $sceDelegateProvider.resourceUrlWhitelist(['self', /^https?:\/\/(cdn\.)?quadramma.com/]);
        delete $httpProvider.defaults.headers.common['X-Requested-With'];
    }
])


.run([
    '$QJConfig',
    function($QJConfig) {
        //store.clear();
        $QJConfig.configure();
    }
])


.controller('AppController', function(
    $QJLogger, $QJHelperFunctions, $scope, $rootScope, $QJLoginModule, $QJApi, $timeout, $state, $QJLoginModule
) {
    $QJLogger.log("AppController -> initialized");
    //$QJHelperFunctions.checkAPIAndGoToApiErrorStateIfThereIsAProblem();
    $QJHelperFunctions.checkTokenExpirationAndGoToLoginStateIfHasExpired();
})

.controller('NavController', function(
    $QJLogger, $QJHelperFunctions, $QJApi,
    $scope, $rootScope, $QJLoginModule, $QJLocalSession, $QJConfig) {
    $QJLogger.log("NavController -> initialized");

    //Siempre que entra al home recupera los datos del usuario actual y los setea globalmente en el rootScope.
    $QJApi.getController('user').get({
        action: 'current'
    }, function(res) {
        $QJLogger.log("HomeController -> user -> api get -> user single -> success");
        $rootScope.currentUser = res.user;
        $rootScope.session.user = res.user;
        $rootScope.$emit('currentUser.change');
        //console.info(res);


       

    });

    $scope.signout = function() {
        $rootScope.session.token = null;
        store.clear();
        $QJHelperFunctions.changeState('login');
        $QJLogger.log("NavController -> signout -> at " + new Date());
    }
})



.controller('SidebarController', function(
    $QJLogger, $scope, $rootScope, $QJLoginModule, $QJLocalSession, $QJConfig, $QJApi) {
    $QJLogger.log("SidebarController -> initialized");

    function getNodesForCurrentToken() {
        //Siempre que carga el sidebar recupera el menu para el usuario
        $QJApi.getController('module').get({
            action: 'menu'
        }, function(res) {
            $QJLogger.log("SidebarController -> api get -> module menu -> success");
            //console.info(res);
            $scope.modules = res.modules;
        });
    }

    $rootScope.$on('session.change', function(args1, args2) {
        getNodesForCurrentToken();
    });

    getNodesForCurrentToken();
})


.controller('HomeController', function(
    $QJAuth, $QJCCombobox, $QJLogger, $scope, $rootScope, $QJLoginModule, $QJLocalSession, $QJConfig, $QJApi) {
    $QJLogger.log("HomeController -> initialized");

   $scope.breadcrumb = {
            name:'Dashboard',
            list:[
                //{name:"None1",state:'module-project-list',fa:'fa-dashboard'},
                //{name:'None2',state:'',fa:'fa-dashboard'}
            ],
            active : "Dashboard"
        };


})



//COMMON DIRECTIVES



;