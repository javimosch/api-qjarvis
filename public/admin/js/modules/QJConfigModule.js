angular.module('QJConfigModule', [])

.factory('$QJConfig', [
	'$QJLogger', '$rootScope', '$state', '$timeout', '$QJLocalSession', '$QJAuth',
	function($QJLogger, $rootScope, $state, $timeout, $QJLocalSession, $QJAuth) {
		var self = {
			appName: 'QJ',
			AppIdentifier: "AppIdentifier_NAME",
			//api: "http://localhost/qjarvis/api", //SIN '/' AL FINAL
			//api: "http://www.quadramma.com/pruebas/qjarvis/api", //SIN '/' AL FINAL  
			api: 
				(location.origin + location.pathname).toString().replace("admin","api").substring(0,(location.origin 
				+ location.pathname).toString().replace("admin","api").length-1), //API IN SAME PLACE (admin,api) //SIN '/' AL FINAL
			facebookAppID: "815991785078819",
			_group_id: 2, //DEFAULT QJARVIS BACKEND (2)
			listviewEntriesPerPage: 5,
			htmlTitle: "QJarvis | Dashboard"
		};
		return {
			configure: function() {
				$rootScope.config = self;
				var localstoreSessionData = $QJLocalSession.load();
				session = localstoreSessionData;
				
				if((session&&session._group_id)){
					session.config = self;	
				}
				//
				self._group_id = (session&&session._group_id) ? session._group_id : self._group_id; //updates config with session _group_id
				if (localstoreSessionData) {
					$rootScope.session = localstoreSessionData;
					$QJLocalSession.save();
					$QJLogger.log('QJConfig-> configure-> session initialized from localstore');
				} else {
					$QJLogger.log('QJConfig-> configure-> session initialized from zero');
					$rootScope.session = {
						loginname: "",
						token: null,
						tokenReq: null,
						tokenExp: null,
					};
				}
				//
				$rootScope.htmlTitle = $rootScope.config.htmlTitle;
				//
				$QJLogger.log('QJConfig-> configure-> success');
			}
		};
	}
])


.factory('$QJAuth', ['$QJLogger', "$rootScope", "$http", '$QJLocalSession',
	function($QJLogger, $rootScope, $http, $QJLocalSession) {
		return {
			updateSessionCustom: function(token, _group_id) {
				$rootScope.session.token = token;
				$rootScope.session._group_id = _group_id;
				$rootScope.config._group_id = _group_id;
				$QJLocalSession.save();
				$rootScope.$emit('session.change');
				$QJLogger.log('QJAuth -> updateSessionCustom -> token ->' + token);
			},
			updateSessionFromLogin : function(res) {
				$rootScope.session.loginname = res.loginname;
				$rootScope.session.token = res.token;
				$rootScope.session.tokenReq = res.tokenReq;
				$rootScope.session.tokenExp = res.tokenExp;
				$rootScope.session._group_id = $rootScope.config._group_id;
				$QJLocalSession.save();
				$rootScope.$emit('session.change');
				$QJLogger.log('QJAuth -> updateSessionFromLogin -> token ->' + res.token);

			}
		}
	}
])

.factory('$QJLocalSession', [
	'$rootScope','$http',
	function($rootScope,$http) {
		return {
			load: function() {
				return store.get("qj_" + $rootScope.config.AppIdentifier + "_session") || null;
			},
			save: function() {
				$http.defaults.headers.common['auth-token'] = $rootScope.session.token;
				store.set("qj_" + $rootScope.config.AppIdentifier + "_token", $rootScope.session.token);
				store.set("qj_" + $rootScope.config.AppIdentifier + "_session", $rootScope.session);
				session = $rootScope.session;
			}
		}
	}
])





;