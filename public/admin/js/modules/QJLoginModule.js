angular.module('QJLoginModule', [
	"QJConfigModule", "QJApiModule", "QJHelperFunctionsModule", "ngResource"
]).factory('$QJLoginModule', [

	'$QJLogger', '$QJAuth', "$QJConfig", "$QJApi", "$resource", "$rootScope", '$QJLocalSession',
	function($QJLogger, $QJAuth, $QJConfig, $QJApi, $resource, $rootScope, $QJLocalSession) {
		var rta = new(function() {
			//--CLASS DEF
			var self = this;
			//
			self.login = function(loginname, password, success, failure) {
				var reqData = {
					"loginname": loginname,
					"password": password,
					"tokenReq": new Date().getTime(),
					'_group_id': $rootScope.config._group_id,
				};
				$QJLogger.log('QJLoginModule -> reqData');
				//console.info(reqData);
				var Auth = $QJApi.getController("auth");
				Auth.post({
					action: "login"
				}, reqData, function(res) {
					$QJLogger.log('QJLogin -> success');
					$QJAuth.updateSessionFromLogin(res);
					success();
				});
			};
			return self;
			//--CLASS DEF
		})();
		return rta; //factory return
	}
])



.controller('LoginController', function(
	$QJLogger,
	$scope, $rootScope, $QJLoginModule, $timeout, $QJHelperFunctions) {
	$QJLogger.log('LoginController');

	$scope.loginnameRequired = false;
	$scope.passwordRequired = false;

	setTimeout(function() {
		$rootScope.error = {
			message: ""
		};
	}, 4000);


	$scope.classForPassword = function() {
		return 'form-group ' + ($scope.passwordRequired ? 'has-error' : '');
	};

	$scope.invalidCredentials = function() {
		console.info("[QJarvisAppLoginController]->[InvalidCredentials]");
		$scope.showError("Credenciales invalidas");
	};

	$scope.showError = function(errorMessage) {
		$rootScope.error = {
			message: errorMessage
		};
		setTimeout(function() {
			$rootScope.message = '';
		}, 5000);
	};

	$scope.validateFields = function(success) {
		if (_.isUndefined($scope.loginname) || $scope.loginname == "") {
			console.info("[]->[loginname required]");
			$scope.showError("Usuario requerido");
		} else {
			if (_.isUndefined($scope.password) || $scope.password == "") {
				console.info("[]->[password required]");
				$scope.showError("Password requerida");
			} else {
				success();
			}
		}
	};

	$scope.submit = function() {
		$scope.validateFields(function() {
			$QJLoginModule.login($scope.loginname, $scope.password, function() {
				$QJHelperFunctions.changeState('home');
			});
		});
	};
})

;