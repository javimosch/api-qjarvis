angular.module("QJControlsModule", [])

.factory('$QJCFilter', [
	'$QJLogger', '$rootScope', '$state', '$timeout', '$QJLocalSession', '$QJAuth',
	function($QJLogger, $rootScope, $state, $timeout, $QJLocalSession, $QJAuth) {
		var self = {
			fields: {}
		};

		function getBindedArray(arrayName, $scope, obj, index) {
			if (index == 0) {
				$QJLogger.log('QJCFilter -> getBindedArray -> something went wrong and i abort the recursive func bro!');
			}
			if (!_.isUndefined(obj) && _.isNull(obj)) {
				return obj;
			}
			if (arrayName.toString().split('.').length == 1 || index == 0) {
				//console.info(arrayName);
				if (!_.isUndefined(obj)) {
					return obj[arrayName] || null;
				} else {
					//console.info('return this ->'+arrayName);
					//console.info($scope[arrayName]);
					return $scope[arrayName] || null;
				}

			} else {
				var firstPart = arrayName.toString().split('.')[0];
				var rest = arrayName.substring(firstPart.length + 1);
				//console.info(arrayName);
				return getBindedArray(rest, $scope, $scope[firstPart], (_.isUndefined(index) ? 20 : index--));
			}

		};
		return {
			create: function(settings, $scope) {
				_.each(settings.fields, function(field, key) {
					self.fields[field.name] = null;
				});

				//defaults
				settings.filteredfieldName = settings.filteredfieldName || '_qjfiltered';

				//stores settings as property
				self.settings = settings;
				$scope[settings.name] = self;

				self.filter = function() {
					//console.clear();
					containValidationSuccessItemsKeys = [];
					_.each(self.fields, function(val, key) {
						var keyWhoChanges = key; //updates based on all filters ! fix
						var newFieldValue = val;
						_.each(settings.fields, function(field, key) {
							if (keyWhoChanges !== field.name) return; //take only the one who changes
							var bindedArray = getBindedArray(field.arrayName, $scope);
							if (bindedArray !== null) {
								_.each(bindedArray, function(bindedArrayItem, bindedArrayItemKey) {
									bindedArrayItemHasSuccessAny = (null != _.find(containValidationSuccessItemsKeys, function(val) {
										return val == bindedArrayItemKey
									}));
									if (bindedArrayItemHasSuccessAny) {
										return; // jump because alredy succes validation and it not gonna be filtered
									}
									var containValidationResponse = [];
									_.each(field.bindTo, function(bindToField, key) {
										var _field = bindedArrayItem[bindToField];
										if (!_.isUndefined(_field)) {
											if (_field !== null) {
												var flag = true;
												if (_.isUndefined(newFieldValue) || _.isNull(newFieldValue) || newFieldValue == "") {
													return; // jump because filter field is empty!
												} else {
													var indexof = _field.toString().toLowerCase().indexOf(newFieldValue.toString().toLowerCase());
													if (indexof !== -1) {
														flag = true;
													} else {
														flag = false;
													}
												}
												containValidationResponse.push(flag);

											} else {
												$QJLogger.log("QJCFilter -> Warning -> bindedArrayItem " + bindToField + " at index " + bindedArrayItemKey + " is null so its omited from filtering");
											}
										} else {
											$QJLogger.log("QJCFilter -> Warning -> bindedArrayItem " + bindToField + " do not exists in " + field.arrayName);
										}
									});
									var passContainValidation = (null != _.find(containValidationResponse, function(val) {
										return val == true
									}));
									bindedArrayItem[settings.filteredfieldName] = !passContainValidation;
									if (containValidationResponse.length == 0) {
										bindedArrayItem[settings.filteredfieldName] = false; //no hubo respuestas por lo tanto no se filtra
									}
									if (bindedArrayItem[settings.filteredfieldName]) {
										containValidationSuccessItemsKeys.push(bindedArrayItemKey); //si se filtra una ves jump para el resto
									}
								});
							} else {
								$QJLogger.log("QJCFilter -> Warning -> arrayName " + field.arrayName + " for filter field " + field.name + " do not exists on the scope");
							}
						});
					});
					$scope.$emit('qjcfilter.update', {
						filteredfieldName: settings.filteredfieldName
					});
				};
				$scope.$watch(settings.name + '.fields', function(newValue, oldValue) {
					self.filter();
				}, true);
			}
		}
	}
])


.factory('$QJCListview', [
	'$QJApi', '$QJHelperFunctions', '$QJLogger', '$rootScope', '$state', '$timeout', '$QJLocalSession', '$QJAuth',
	function($QJApi, $QJHelperFunctions, $QJLogger, $rootScope, $state, $timeout, $QJLocalSession, $QJAuth) {


		function createPagedList(items, entriesPerPage) {
			var pagesCounter = 1;
			var pages = [];
			//
			var _currItemIndex = 0;
			var _currPage = [];
			while (_currItemIndex < items.length) { //ej: 0 < 5
				if (_currPage.length < entriesPerPage) {
					_currPage.push(items[_currItemIndex]);
					_currItemIndex++;
				} else {
					pages.push(_currPage);
					_currPage = [];
					pagesCounter++;
				}
			}
			if (_currPage.length > 0) {
				pages.push(_currPage);
			}
			return pages;
		}

		function buildListViewData(items) {
			var entriesPerPage = $rootScope.config.listviewEntriesPerPage; //ej: 2   
			var pages = [];
			if (!_.isUndefined(items)) {
				pages = createPagedList(items, entriesPerPage);
			}
			var pageNumbers = [];
			_.each(pages, function(e, index) {
				pageNumbers.push(index + 1);
			});
			var _lvData = {
				currentPageIndex: 0,
				currentPage: pages[0],
				totalPages: pages.length,
				totalItems: items.length,
				pages: pages,
				pagination: {
					pageNumbers: pageNumbers,
					disabledForPrevLink: function() {
						return _lvData.currentPageIndex === 0 ? true : false;
					},
					disabledForNextLink: function() {
						return _lvData.currentPageIndex >= pages.length - 1 ? true : false;
					},
					activeForLink: function(pageNumber) {
						if ((pageNumber === _lvData.currentPageIndex + 1)) {
							return true;
						} else {
							return false;
						}
					},
					goto: function(pageNumber) {
						_lvData.currentPageIndex = pageNumber - 1;
						_lvData.currentPage = pages[_lvData.currentPageIndex];
					},
					next: function() {
						_lvData.currentPageIndex++;
						if (_lvData.currentPageIndex >= pages.length) {
							_lvData.currentPageIndex = pages.length - 1;
						}
						_lvData.currentPage = pages[_lvData.currentPageIndex];
					},
					prev: function() {
						_lvData.currentPageIndex--;
						if (_lvData.currentPageIndex <= 0) {
							_lvData.currentPageIndex = 0;
						}
						_lvData.currentPage = pages[_lvData.currentPageIndex];
					}
				}
			};
			return _lvData;
		}
		return {
			create: function(settings, $scope) {
				//instance private
				function render(items) {
					$scope[settings.pagedDataArray] = buildListViewData(items);
				}



				//watch
				$scope.$watch(settings.dataArray, function(newValue, oldValue) {

					if (_.isUndefined($scope[settings.dataArray])) {
						$QJLogger.log("WARNING: QJCListview -> " + settings.dataArray + " -> " + " dataArray undefined");
						return;
					}

					$scope[settings.pagedDataArray] = buildListViewData($scope[settings.dataArray]);
					render($scope[settings.dataArray]);
				});


				$scope.$on('qjcfilter.update', function(args1, args2) {
					$scope.$emit(settings.name + ".update", {});
					var filteredData = _.filter($scope[settings.dataArray], function(item) {
						return !item[args2.filteredfieldName];
					});
					render(filteredData);

					var filteredCount = _.filter($scope[settings.dataArray], function(item) {
						return item[args2.filteredfieldName] == true;
					});
					$scope.$emit('qjclistview.filter.success', {
						filteredCount: filteredCount
					});

				});

				var self = settings;
				$scope[settings.name] = self;

				self.update = function() {
					//DB
					$QJApi.getController(settings.api.controller).get(settings.api.params, function(res) {
						$QJLogger.log("QJCListview -> " + settings.api.controller + " " + settings.api.params.action + " -> success");
						$scope[settings.dataArray] = res.items;
						$scope.$emit(settings.name + ".update", {});
						//console.info($scope[settings.dataArray]);
					});
					//$scope.$emit(settings.name+".update",{});
				};
				self.update();


			}
		};
	}
])


.directive('qjclistview', function() {
	var directive = {};
	directive.restrict = 'E'; /* restrict this directive to elements */
	directive.templateUrl = "pages/controls/qjclistview.html";
	directive.scope = {
		data: "=",
		lvw: "="
	}
	directive.compile = function(element, attributes) {
		var linkFunction = function($scope, element, attributes) {}
		return linkFunction;
	}
	return directive;
})


//**************************************************************************************************
//**************************************************************************************************
//**************************************************************************************************
//**************************************************************************************************
//SELECTKEY ****************************************************************************************
.factory('$QJCSelectkey', [
	'$QJHelperFunctions', '$QJLogger', '$rootScope', '$state', '$timeout', '$QJLocalSession', '$QJAuth',
	function($QJHelperFunctions, $QJLogger, $rootScope, $state, $timeout, $QJLocalSession, $QJAuth) {
		function seekObject(fullname, $scope, obj, index) {
			if (index == 0) {
				$QJLogger.log('QJCSelectkey -> seekObject -> something went wrong and i abort the recursive func bro!');
			}
			if (!_.isUndefined(obj) && _.isNull(obj)) {
				return obj;
			}
			if (fullname.toString().split('.').length == 1 || index == 0) {
				if (!_.isUndefined(obj)) {
					return obj[fullname] || null;
				} else {
					return $scope[fullname] || null;
				}

			} else {
				var firstPart = fullname.toString().split('.')[0];
				var rest = fullname.substring(firstPart.length + 1);
				return seekObject(rest, $scope, $scope[firstPart], (_.isUndefined(index) ? 20 : index--));
			}
		};
		return {
			create: function(settings, $scope) {
				settings.code_copyto = settings.code_copyto || null;

				var self = settings;
				$scope[settings.name] = self;

				if (self.code_copyto != null) {
					var cuts = self.code_copyto.toString().split('.');
					var fieldWord = cuts[cuts.length - 1];
					var pos = self.code_copyto.toString().indexOf('.' + fieldWord);
					var path = self.code_copyto.toString().substring(0, pos);
					var obj = seekObject(path, $scope);
					$scope.$watch(settings.name + '.code', function(newVal, oldVal) {
						obj[fieldWord] = newVal;
					});
					obj[fieldWord] = self.code;
				}


			}
		};
	}
])
	.directive('qjcselectkey', function($rootScope) {
		var directive = {};
		directive.restrict = 'E'; /* restrict this directive to elements */
		directive.templateUrl = "pages/controls/qjcselectkey.html";
		directive.scope = {
			slk: '='
		};
		directive.compile = function(element, attributes) {
			var linkFunction = function($scope, element, attributes) {}
			return linkFunction;
		}
		return directive;
	})



//**************************************************************************************************
//**************************************************************************************************
//**************************************************************************************************
//**************************************************************************************************
//QJC COMBOBOX ****************************************************************************************
.factory('$QJCCombobox', [
	'$QJApi', '$QJHelperFunctions', '$QJLogger', '$rootScope', '$state', '$timeout', '$QJLocalSession', '$QJAuth',
	function($QJApi, $QJHelperFunctions, $QJLogger, $rootScope, $state, $timeout, $QJLocalSession, $QJAuth) {
		function seekObject(fullname, $scope, obj, index) {
			if (index == 0) {
				$QJLogger.log('QJCSelectkey -> seekObject -> something went wrong and i abort the recursive func bro!');
			}
			if (!_.isUndefined(obj) && _.isNull(obj)) {
				return obj;
			}
			if (fullname.toString().split('.').length == 1 || index == 0) {
				if (!_.isUndefined(obj)) {
					return obj[fullname] || null;
				} else {
					return $scope[fullname] || null;
				}

			} else {
				var firstPart = fullname.toString().split('.')[0];
				var rest = fullname.substring(firstPart.length + 1);
				//console.log("obj ->"+obj);
				//console.log("firstpart->"+firstPart);
				//console.log("rest->"+rest);
				return seekObject(rest, $scope, obj != null ? obj[firstPart] : $scope[firstPart], (_.isUndefined(index) ? 20 : index--));
			}
		};
		return {
			create: function(settings, $scope) {

				/*
				console.info('QJCCombobox ->  LOAD '
					+ ' CODE['+settings.code+']'
				);
*/

				settings.code_copyto = settings.code_copyto || null;
				settings.description_copyto = settings.description_copyto || null;


				var self = settings;

				self.initialValue = settings.code;
				self.selectedValue = self.selectedValue || -1;
				self.disabled = self.disabled || false;

				self.ngSelected = function(item) {
					return item._id == self.initialValue;
				};

				$scope[settings.name] = self; //sets to the scope !!!!

				if (typeof cbo == "undefined") {
					cbo = [];
				}
				cbo.push(self);

				$scope.$watch(settings.name + ".selectedValue", function(newVal, oldVal) {
					self.code = newVal;
					$scope.$emit(settings.name + '.change', {
						selectedValue: newVal
					});
				});
				$scope.$watch(settings.name + ".code", function(newVal, oldVal) {
					self.selectedValue = newVal;

					self.description = (_.find(self.items, function(item) {
						return item._id == newVal;
					}));
					self.description = self.description && self.description.description || "";

					$scope.$emit(settings.name + '.change', {
						selectedValue: newVal
					});
				});

				function copy(obj, fieldWord, val) {
					if (_.isUndefined(val)) {
						return;
					}
					if (val.toString() === '-1') {
						obj[fieldWord] = '';
					} else {
						obj[fieldWord] = val;
					}
				}

				function copyWhenPosible(fullpath, val) {
					if (_.isUndefined(fullpath) || _.isNull(fullpath) || fullpath.length == 0) {
						return; //omit!
					}
					var cuts = fullpath.toString().split('.');
					var fieldWord = cuts[cuts.length - 1];
					var pos = fullpath.toString().indexOf('.' + fieldWord);
					var path = fullpath.toString().substring(0, pos);
					//console.info("seeking for path obj on _>>>> "+path);
					var obj = seekObject(path, $scope);
					//console.info("founded "+JSON.stringify(obj));
					if (_.isUndefined(obj) || _.isNull(obj)) {
						console.info("copyWhenPosible failure for path -> " + fullpath);
						return; //omit!
					}
					copy(obj, fieldWord, val);
				}


				$scope.$watch(settings.name + '.code', function(newVal, oldVal) {
					copyWhenPosible(self.code_copyto, newVal);
				});
				copyWhenPosible(self.code_copyto, self.code || '');



				//set defaults
				$scope.$emit(settings.name + '.change', {
					selectedValue: self.code
				});

				if (self.description_copyto != null) {
					var cuts = self.description_copyto.toString().split('.');
					self.description_copyto_fieldWord = cuts[cuts.length - 1];
					var pos = self.description_copyto.toString().indexOf('.' + self.description_copyto_fieldWord);
					var path = self.description_copyto.toString().substring(0, pos);
					self.description_copyto_obj = seekObject(path, $scope);
					$scope.$watch(settings.name + '.description', function(newVal, oldVal) {
						copy(self.description_copyto_obj, self.description_copyto_fieldWord, newVal);
					});
					copy(self.description_copyto_obj, self.description_copyto_fieldWord, self.description || '');
					$scope.$emit(settings.name + '.description', {
						description: self.description
					});
				}


				self.update = function() {
					$QJApi.getController(settings.api.controller).get(settings.api.params, function(res) {
						//$QJLogger.log("QJCCombobox -> "+settings.name+" -> " + settings.api.controller + "  " + settings.api.params.action + " ("+JSON.stringify(settings.api.params)+") -> success");
						self.items = res.items;
						self.selectedValue = self.initialValue;
						//console.info(res.req);
					});
				};
				self.update(); //initial

				//watch for params change to update
				$scope.$watch(settings.name + '.api.params', function(newVal, oldVal) {
					self.update();
					//$QJLogger.log("QJCCombobox -> " + settings.name + " -> params changes -> updating..");
				}, true);


			}
		};
	}
])
	.directive('qjccombobox', function($rootScope) {
		var directive = {};
		directive.restrict = 'E'; /* restrict this directive to elements */
		directive.templateUrl = "pages/controls/qjccombobox.html";
		directive.scope = {
			cbo: '='
		};
		directive.compile = function(element, attributes) {
			var linkFunction = function($scope, element, attributes) {}
			return linkFunction;
		}
		return directive;
	})

//**************************************************************************************************
//**************************************************************************************************
//**************************************************************************************************
//**************************************************************************************************
//QJC PROJECT HOUR COUNTER*********************************************************************
.factory('$QJCTimeCounter', [
	'$interval', '$QJApi', '$QJHelperFunctions', '$QJLogger', '$rootScope', '$state', '$timeout', '$QJLocalSession', '$QJAuth',
	function($interval, $QJApi, $QJHelperFunctions, $QJLogger, $rootScope, $state, $timeout, $QJLocalSession, $QJAuth) {
		return {
			create: function(settings, $scope) {
				var self = _.extend(settings, {
					working: false,
					project: "none",
					startTimeFormated: null,
					endTimeFormated: null,
					errors: [],
					callingApi : false
				});
				self.addError = function(error) {
					self.errors.push(error);
					$timeout(function() {
						$scope.$apply(function() {
							self.errors = [];
						});
					}, 2000);
				};
				self.restart = function() {
					self.startTimeFormated = null;
					self.endTimeFormated = null;
					self.errors = [];
					self.diffFormated = null;
				};
				self.init = function() {
					if(self.callingApi) return; //calling apy sync please.
					self.restart();
					self.callingApi = true;
					$QJApi.getController(settings.api.controller).get(settings.api.params, function(res) {
						self.callingApi = false;
						$QJLogger.log("QJCTimeCounter -> " + JSON.stringify(settings.api) + " -> success");
						self.working = (res.item != null);
						self.resitem = res.item;
						if (!_.isUndefined(settings.onInit)) {
							settings.onInit(self);
						}
					});
					return self;
				};
				self.getTime = function() {
					return new Date().getTime();
				};
				self.getTimeFormated = function() {
					return moment(self.getTime()).format("dddd, MMMM Do YYYY, h:mm:ss a");
				};
				self.getDiff = function(milli) {
					var actual = self.getTime();
					return (actual - milli);
				};
				self.getDiffFormated = function(milli) {
					var diff = self.getDiff(milli);
					var duration = {
						hours: Math.round((diff / 1000 / 60 / 60) % 24),
						minutes: Math.round((diff / 1000 / 60) % 60),
						seconds: Math.round((diff / 1000) % 60)
					};
					var str = "";
					str += duration.hours + " hours, ";
					str += duration.minutes + " mins, ";
					str += duration.seconds + " secs, ";
					//str += diff + " total, ";
					return str;
				};
				self.validateStart = function() {
					if (!_.isUndefined(settings.onValidateStart)) {
						return settings.onValidateStart(self);
					} else {
						return true;
					}
				};
				self.resume = function(from) {
					self.start(from);
				};
				self.start = function(start) {
					if (!self.validateStart()) {
						return;
					}else{
						//console.info("TIMER STARTED FAIL");
					}

					//
					//console.info("TIMER STARTED");

					if (start && start.length > 0) {
						self._startVal = parseInt(start);
					} else {
						self._startVal = self.getTime(); //start setted	
					}

					if (!_.isUndefined(settings.onStartChange)) {
						settings.onStartChange(self._startVal, self);
					}
					self.startTimeFormated = self.getTimeFormated(); //start formated setted
					self.endTimeFormated = self.startTimeFormated; //end setted
					self.diff = self.getDiff(self._startVal);
					self.diffFormated = self.getDiffFormated(self._startVal);
					if (!_.isUndefined(settings.onDiffChange)) {
						settings.onDiffChange(self.diff, self.diffFormated, self);
					}
					self.workingInterval = $interval(function() {
						if (!self.working) return;
						self._stopVal = self.getTime();
						if (!_.isUndefined(settings.onStopChange)) {
							settings.onStopChange(self._stopVal, self);
						}
						self.endTimeFormated = self.getTimeFormated();
						self.diff = self.getDiff(self._startVal);
						self.diffFormated = self.getDiffFormated(self._startVal);
						if (!_.isUndefined(settings.onDiffChange)) {
							settings.onDiffChange(self.diff, self.diffFormated, self);
						}
					}, 1000);
					self.working = true;
					if (!_.isUndefined(settings.onStartClick)) {
						settings.onStartClick(self);
					}
				};
				self.stop = function() {
					self.working = false;
					$interval.cancel(self.workingInterval);
					if (!_.isUndefined(settings.onStopClick)) {
						settings.onStopClick(self);
					}
				};
				$scope[settings.name] = self;
				return self;
			}
		};
	}
]);



//------------------- THE END
;