-- phpMyAdmin SQL Dump
-- version 4.0.8
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 26-07-2014 a las 08:56:06
-- Versión del servidor: 5.1.73-community
-- Versión de PHP: 5.2.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `lc000386_qjarvis`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `qj_album`
--

CREATE TABLE IF NOT EXISTS `qj_album` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `_company_id` int(11) NOT NULL,
  `description` varchar(50) NOT NULL,
  `_image_id` int(11) NOT NULL,
  PRIMARY KEY (`_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `qj_album`
--

INSERT INTO `qj_album` (`_id`, `_company_id`, `description`, `_image_id`) VALUES
(1, 5, 'Album Zapatos', 11),
(2, 9, 'Album Perfumes', 12);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `qj_album_item`
--

CREATE TABLE IF NOT EXISTS `qj_album_item` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `_album_id` int(11) NOT NULL,
  `description` varchar(50) NOT NULL,
  `_image_id` int(11) NOT NULL,
  PRIMARY KEY (`_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Volcado de datos para la tabla `qj_album_item`
--

INSERT INTO `qj_album_item` (`_id`, `_album_id`, `description`, `_image_id`) VALUES
(1, 1, 'Zapato 1', 3),
(2, 1, 'Zapato 2', 4),
(3, 1, 'Zapato 3', 5),
(4, 1, 'Zapato 4', 6),
(5, 2, 'Perfume 1', 7),
(6, 2, 'Perfume 2', 8),
(7, 2, 'Perfume 3', 9),
(8, 2, 'Perfume 4', 10);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `qj_brand`
--

CREATE TABLE IF NOT EXISTS `qj_brand` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `_company_id` int(11) NOT NULL,
  `description` varchar(50) NOT NULL,
  `_image_id` int(11) NOT NULL,
  PRIMARY KEY (`_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `qj_brand`
--

INSERT INTO `qj_brand` (`_id`, `_company_id`, `description`, `_image_id`) VALUES
(1, 5, 'Miss Pies Brand', 11),
(2, 9, 'Gabanna Brand', 12);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `qj_category`
--

CREATE TABLE IF NOT EXISTS `qj_category` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `_company_id` int(11) NOT NULL,
  `_category_type_id` int(11) NOT NULL,
  `description` varchar(50) NOT NULL,
  `path` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Volcado de datos para la tabla `qj_category`
--

INSERT INTO `qj_category` (`_id`, `_company_id`, `_category_type_id`, `description`, `path`) VALUES
(7, 5, 4, 'Brand logos', 'http://www.quadramma.com//pruebas/qjarvis/api/file/images'),
(3, 5, 5, 'Categoria Zapatos 1', 'www.quadramma.com/pruebas/qj/uploads/misspiess_collection_cat1'),
(4, 5, 5, 'Categoria Zapatos 2', 'www.quadramma.com/pruebas/qj/uploads/misspiess_collection_cat2'),
(5, 9, 5, 'Categoria Perfumes 1', 'www.quadramma.com/pruebas/qj/uploads/gabanna_collection_cat1'),
(6, 9, 5, 'Categoria Perfumes 2', 'www.quadramma.com/pruebas/qj/uploads/gabanna_collection_cat2'),
(8, 9, 4, 'Brand logos', 'http://www.quadramma.com//pruebas/qjarvis/api/file/images');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `qj_category_type`
--

CREATE TABLE IF NOT EXISTS `qj_category_type` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(50) NOT NULL,
  `_group_id` int(11) NOT NULL,
  PRIMARY KEY (`_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `qj_category_type`
--

INSERT INTO `qj_category_type` (`_id`, `description`, `_group_id`) VALUES
(1, 'Vimoda Collection Images', 3),
(3, 'Vimoda Lookbook Images', 3),
(4, 'Vimoda Brand Logo', 3),
(5, 'Vimoda Collection Categories', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `qj_channel`
--

CREATE TABLE IF NOT EXISTS `qj_channel` (
  `_id` int(11) NOT NULL,
  `description` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `qj_channel`
--

INSERT INTO `qj_channel` (`_id`, `description`) VALUES
(1, 'Chat -> Todos');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `qj_chat`
--

CREATE TABLE IF NOT EXISTS `qj_chat` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(50) NOT NULL,
  `_usergroup_id` int(11) NOT NULL,
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `qj_chat`
--

INSERT INTO `qj_chat` (`_id`, `description`, `_usergroup_id`) VALUES
(1, 'QM -> Vipster', 1),
(2, 'QM -> Rise', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `qj_chat_message`
--

CREATE TABLE IF NOT EXISTS `qj_chat_message` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `_chat_id` int(11) NOT NULL,
  `_user_id` int(11) NOT NULL,
  `message` varchar(20000) NOT NULL,
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Volcado de datos para la tabla `qj_chat_message`
--

INSERT INTO `qj_chat_message` (`_id`, `_chat_id`, `_user_id`, `message`) VALUES
(1, 1, 1, 'Hola buba!'),
(2, 1, 2, 'Hola !'),
(3, 1, 1, 'asd'),
(4, 1, 1, 'qwe'),
(5, 1, 1, 'dfg'),
(6, 1, 1, 'qwe'),
(7, 1, 1, 'rty'),
(8, 1, 1, 'pepe'),
(9, 1, 5, 'a ver si funca el chat');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `qj_company`
--

CREATE TABLE IF NOT EXISTS `qj_company` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(50) NOT NULL,
  PRIMARY KEY (`_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Volcado de datos para la tabla `qj_company`
--

INSERT INTO `qj_company` (`_id`, `description`) VALUES
(1, 'Quadramma'),
(4, 'Amblagar Studios'),
(5, 'Miss Pies'),
(6, 'Ga iluminacion'),
(7, 'Bizonti '),
(8, 'Vimoda'),
(9, 'Dolce & Gabbana');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `qj_group`
--

CREATE TABLE IF NOT EXISTS `qj_group` (
  `_id` int(11) NOT NULL,
  `description` varchar(50) NOT NULL,
  PRIMARY KEY (`_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `qj_group`
--

INSERT INTO `qj_group` (`_id`, `description`) VALUES
(1, 'App BERP Sytem'),
(2, 'App QJarvis Backend'),
(4, 'App Fin del mundo'),
(3, 'App Vipster');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `qj_image`
--

CREATE TABLE IF NOT EXISTS `qj_image` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `_category_id` int(11) NOT NULL,
  `filename` varchar(50) NOT NULL,
  `description` varchar(50) NOT NULL,
  `bytes` longblob,
  PRIMARY KEY (`_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Volcado de datos para la tabla `qj_image`
--

INSERT INTO `qj_image` (`_id`, `_category_id`, `filename`, `description`, `bytes`) VALUES
(7, 5, '1.jpg', 'Perfume descripcion', NULL),
(3, 3, '1.jpg', 'Zapato descripcion', NULL),
(4, 3, '2.jpg', 'Zapato descripcion', NULL),
(5, 4, '3.jpg', 'Zapato descripcion', NULL),
(6, 4, '4.jpg', 'Zapato descripcion', NULL),
(8, 5, '2.jpg', 'Perfume descripcion', NULL),
(9, 6, '3.jpg', 'Perfume descripcion', NULL),
(10, 6, '4.jpg', 'Perfume descripcion', NULL),
(11, 7, 'logo1.jpg', 'Miss Pies Logo', NULL),
(12, 8, 'logo2.jpg', 'Gabanna logo', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `qj_menu`
--

CREATE TABLE IF NOT EXISTS `qj_menu` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(50) NOT NULL,
  `_profile_id` int(11) NOT NULL,
  `_group_id` int(11) NOT NULL,
  PRIMARY KEY (`_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Volcado de datos para la tabla `qj_menu`
--

INSERT INTO `qj_menu` (`_id`, `description`, `_profile_id`, `_group_id`) VALUES
(3, 'QJB / Admin', 1, 2),
(4, 'QJB / Admin / Vipster', 1, 3),
(5, 'VIMODA / Brand Users', 2, 3),
(6, 'VIMODA / Users', 3, 3),
(8, 'BERP / Admin / Editorial Ariel', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `qj_menu_module`
--

CREATE TABLE IF NOT EXISTS `qj_menu_module` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `_menu_id` int(11) NOT NULL,
  `_module_id` int(11) NOT NULL,
  PRIMARY KEY (`_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Volcado de datos para la tabla `qj_menu_module`
--

INSERT INTO `qj_menu_module` (`_id`, `_menu_id`, `_module_id`) VALUES
(1, 3, 2),
(3, 4, 9),
(4, 3, 10),
(5, 3, 11),
(7, 3, 12),
(8, 4, 12),
(9, 3, 13),
(10, 3, 14);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `qj_module`
--

CREATE TABLE IF NOT EXISTS `qj_module` (
  `_id` int(11) NOT NULL,
  `description` varchar(50) NOT NULL,
  `label` varchar(50) NOT NULL,
  `icon` varchar(20) DEFAULT NULL,
  `state` varchar(50) NOT NULL,
  PRIMARY KEY (`_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `qj_module`
--

INSERT INTO `qj_module` (`_id`, `description`, `label`, `icon`, `state`) VALUES
(1, 'QJ BACKEND -> Company CRUD', 'Companies', 'fa-building', 'module-company'),
(2, 'QJ BACKEND -> User CRUD', 'Users', 'fa-users', 'module-user-list'),
(3, 'QJ BACKEND -> Usergroup CRUD', 'User groups', 'fa-users', ''),
(4, 'QJ BACKEND -> Profile CRUD', 'Profiles', 'fa-users', 'module-profile'),
(5, 'QJ VIMODA -> Brands Albums', 'Albums', NULL, ''),
(6, 'QJ VIMODA -> Brand profile', 'My Brand', NULL, ''),
(7, 'QJ VIMODA -> User Albums', 'Albums', NULL, ''),
(8, 'QJ VIMODA -> User profile', '', NULL, ''),
(9, 'QJ VIPSTER -> Admin Settings', 'Vip Settings', 'fa-archive', 'module-vipster-settings'),
(10, 'QJ BACKEND -> MENU CRUD', 'Menus', 'fa-bars', 'module-menu-list'),
(11, 'QJ BACKEND -> QJChat', 'Chat', 'fa-comments ', 'module-chat'),
(13, 'QJ PROJECT Module', 'Projects', 'fa-suitcase ', 'module-project-list'),
(12, 'QJ BACKEND -> Settings', 'Settings', 'fa-wrench', 'module-settings'),
(14, 'QJ PROJECT -> Hours Module', 'Project Hours', 'fa-clock-o', 'module-project-hours-list');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `qj_profile`
--

CREATE TABLE IF NOT EXISTS `qj_profile` (
  `_id` int(11) NOT NULL,
  `description` varchar(50) NOT NULL,
  PRIMARY KEY (`_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `qj_profile`
--

INSERT INTO `qj_profile` (`_id`, `description`) VALUES
(1, 'common -> admin'),
(2, 'vimoda -> brand'),
(3, 'vimoda -> user'),
(4, 'common -> user');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `qj_project`
--

CREATE TABLE IF NOT EXISTS `qj_project` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `_id_company` int(11) NOT NULL,
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Volcado de datos para la tabla `qj_project`
--

INSERT INTO `qj_project` (`_id`, `name`, `description`, `_id_company`) VALUES
(2, 'Vipster', '', 1),
(3, 'Site Endi', 'Website', 1),
(8, 'Selfie jazmin', 'adsadsasd', 5),
(9, 'Rise of Hero', '', 1),
(10, 'Jazmin Chebar Intitucional App', 'Iphone, Ipad, y Android. Galerias Dinamicas para Ipad.', 1),
(11, 'Esfera Universal', 'Interactive Book para Ipad', 1),
(12, 'Contempolatino', '', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `qj_project_hours`
--

CREATE TABLE IF NOT EXISTS `qj_project_hours` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `_id_project` int(11) NOT NULL,
  `_id_user` int(11) NOT NULL,
  `start` varchar(50) NOT NULL,
  `end` varchar(50) DEFAULT NULL,
  `difference` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=67 ;

--
-- Volcado de datos para la tabla `qj_project_hours`
--

INSERT INTO `qj_project_hours` (`_id`, `_id_project`, `_id_user`, `start`, `end`, `difference`) VALUES
(55, 2, 1, '1406125299453', '1406125329607', '30154'),
(56, 2, 1, '1406125299453', '1406125302458', '3005'),
(57, 0, 5, '1406125519026', '1406125570585', '51559'),
(58, 10, 5, '1406125519026', '1406125523031', '4005'),
(59, 10, 5, '1406125546604', '1406125523031', '0'),
(61, 11, 5, '1406126265075', '1406140787563', '14522488'),
(62, 11, 5, '1406126265075', '1406126299080', '34005'),
(63, 11, 5, '1406131703835', '1406126299080', '1'),
(64, 11, 5, '1406131703835', '1406140729081', '9025246'),
(65, 12, 5, '1406140792101', '1406217356082', '76563981'),
(66, 12, 5, '1406140792101', '1406142003082', '1210981');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `qj_social_user`
--

CREATE TABLE IF NOT EXISTS `qj_social_user` (
  `_id` int(11) NOT NULL,
  `_usergroup_id` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  PRIMARY KEY (`_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `qj_social_user`
--

INSERT INTO `qj_social_user` (`_id`, `_usergroup_id`, `first_name`, `last_name`, `email`) VALUES
(12312312, 6, 'Pepe EOTW User', 'El loco', 'ellocopepe@gmail.com'),
(1231231, 6, 'Carlos', 'Tevez', 'tevezelcapo@gmail.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `qj_social_user_stats`
--

CREATE TABLE IF NOT EXISTS `qj_social_user_stats` (
  `_social_user_id` int(11) NOT NULL,
  `clicks` int(11) NOT NULL,
  PRIMARY KEY (`_social_user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `qj_user`
--

CREATE TABLE IF NOT EXISTS `qj_user` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `loginname` varchar(15) NOT NULL,
  `password` varchar(15) NOT NULL,
  `_usergroup_id` int(11) NOT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Volcado de datos para la tabla `qj_user`
--

INSERT INTO `qj_user` (`_id`, `loginname`, `password`, `_usergroup_id`, `first_name`, `last_name`) VALUES
(1, 'javi', '123456', 1, 'Javier Leandro', 'Arancibia'),
(2, 'buba', '123456', 1, 'Gabriel', 'Anderson'),
(3, 'dami', '123456', 1, 'Damian', 'Aliberti'),
(4, 'santi', '123456', 1, 'Santiago', 'Apellido'),
(5, 'ariel', '123456', 1, 'Ariel', 'Chamson'),
(6, 'rolo', '123456', 7, 'Rolo', 'Garcia'),
(7, 'ana', '123456', 7, 'Ana', 'Paula'),
(8, 'paula', '123456', 7, 'Paula', 'Fernandez'),
(9, 'gonzalo', '123456', 7, 'Gonzalo', 'Bravo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `qj_usergroup`
--

CREATE TABLE IF NOT EXISTS `qj_usergroup` (
  `_id` int(11) NOT NULL,
  `description` varchar(50) NOT NULL,
  PRIMARY KEY (`_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `qj_usergroup`
--

INSERT INTO `qj_usergroup` (`_id`, `description`) VALUES
(1, 'Quadramma/Endi -> Admins'),
(2, 'Vimoda Brand Users group'),
(3, 'Vimoda Users group'),
(4, 'Vimoda Brand Users group Jazmin Chebar'),
(5, 'Vimoda Brand Users group Dolce Gabbana'),
(6, 'EOTW FB Users'),
(7, 'RYG Editorial Grafica'),
(8, 'Vipster Usuarios');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `qj_usergroup_company`
--

CREATE TABLE IF NOT EXISTS `qj_usergroup_company` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `_usergroup_id` int(11) NOT NULL,
  `_company_id` int(11) NOT NULL,
  PRIMARY KEY (`_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `qj_usergroup_group`
--

CREATE TABLE IF NOT EXISTS `qj_usergroup_group` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `_usergroup_id` int(11) NOT NULL,
  `_group_id` int(11) NOT NULL,
  `_profile_id` int(11) NOT NULL,
  PRIMARY KEY (`_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Volcado de datos para la tabla `qj_usergroup_group`
--

INSERT INTO `qj_usergroup_group` (`_id`, `_usergroup_id`, `_group_id`, `_profile_id`) VALUES
(1, 1, 2, 1),
(2, 1, 3, 1),
(3, 2, 3, 2),
(4, 3, 3, 3),
(5, 6, 4, 4),
(6, 7, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `qj_usergroup_subgroup`
--

CREATE TABLE IF NOT EXISTS `qj_usergroup_subgroup` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `_usergroup_root_id` int(11) NOT NULL,
  `_usergroup_id` int(11) NOT NULL,
  `_usergroup_subgroup_id` int(11) NOT NULL,
  PRIMARY KEY (`_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `qj_usergroup_subgroup`
--

INSERT INTO `qj_usergroup_subgroup` (`_id`, `_usergroup_root_id`, `_usergroup_id`, `_usergroup_subgroup_id`) VALUES
(1, 2, 2, 4),
(2, 2, 2, 5);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
